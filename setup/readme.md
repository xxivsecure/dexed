This folder contains the files necessary to manually build dexed and its toolchain or to build a dexed release

## Requirements

### Building

- git
- Freepascal ~> 3.2.0
- Lazarus ~> 2.0.12
- ldc2

### Releasing

Same tools as to build plus:

- rpm
- dpkg
- zip

## Build manually dexed and the toolchain

- in the project root directory `bash setup/build-release.sh`

## Building a dexed release

- prepare what will be the new tag, following semantic versioning rules
- update the occurences of the tag in the main readme.
- change the content of the _version.txt_ accordingly.
- commit the changes, for historic reasons the commit message should be "rlz"
- add a git tag on the commit just created.
- in the project root directory run `bash setup/build-release.sh`

The installers are produced in the _output_ directory.
